package com.example.AppliWeb

import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.activity_main_map.*
import java.io.IOException


class MainActivityWelcome : AppCompatActivity() {

    private var auth: FirebaseAuth? = null
    private lateinit var db: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        db = FirebaseFirestore.getInstance()

        setContentView(R.layout.activity_main_welcome)

        auth = FirebaseAuth.getInstance()

        var text_view_res = findViewById(R.id.textViewRes) as TextView

        var btnRetour = findViewById(R.id.buttonRetour) as Button
        var btnEvenements = findViewById(R.id.buttonEvenements) as Button
        var btnLogOut = findViewById(R.id.buttonLogOut) as Button


        var spinner = findViewById(R.id.spinner) as Spinner


        db.collection("villes")
            .get()
            .addOnSuccessListener { result ->
                val colors = Array(result.size()) { _ -> "Aucune expedition" }
                var i = 0
                for (document in result) {
                    Log.d("result", "${document.id} => ${document.data}")
                    document.getString("name")?.let {colors.set(i, it)}
                    i++
                }
                // Initializing an ArrayAdapter
                val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, colors)
                // Set the drop down view resource
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Finally, data bind the spinner object with adapter
                spinner.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.d("error", "Error getting documents: ", exception)
            }


        // Set an on item selected listener for spinner object
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                text_view_res.text = parent.getItemAtPosition(position).toString()
                mapView.onCreate(savedInstanceState)
                mapView.getMapAsync { mapboxMap ->
                    mapboxMap.setStyle(Style.MAPBOX_STREETS) {
                        // Map is set up and the style has loaded. Now you can add data or make other map adjustments
                        var point = getLocationFromAddress(parent.context, text_view_res.text.toString())
                        mapboxMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(point!!, 12.0)
                        )
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }

        btnRetour.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnLogOut.setOnClickListener {
            auth!!.signOut();
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnEvenements.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivityEvenements::class.java)
            intent.putExtra("nameVille", text_view_res.text)
            startActivity(intent)
        }
    }


    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume();
        mapView.onResume();
    }

    override fun onPause() {
        super.onPause();
        mapView.onPause();
    }

    override fun onStop() {
        super.onStop();
        mapView.onStop();
    }

    override fun onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    override fun onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }


    fun getLocationFromAddress(context: Context, strAddress: String): LatLng? {
        val coder = Geocoder(context)
        val address: List<Address>?
        var p1: LatLng? = null

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location = address[0]
            p1 = LatLng(location.latitude, location.longitude)

        } catch (ex: IOException) {

            ex.printStackTrace()
        }
        return p1
    }
}