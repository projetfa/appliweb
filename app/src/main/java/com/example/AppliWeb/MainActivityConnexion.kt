package com.example.AppliWeb

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class MainActivityConnexion : AppCompatActivity() {

    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_connexion)
        auth = FirebaseAuth.getInstance()

        var btnRetour = findViewById(R.id.buttonRetour) as Button
        var btnValid = findViewById(R.id.buttonValid) as Button


        btnRetour.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnValid.setOnClickListener {
            signUpUser()
        }

    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun signUpUser() {

        var editMail = findViewById(R.id.editTextName) as EditText
        var editMdp = findViewById(R.id.editTextMDP) as EditText

        val emailVerify: Boolean = isEmailValid(editMail.text.toString())

        if (!emailVerify) {
            Toast.makeText(applicationContext, "Adresse mail incorrect", Toast.LENGTH_SHORT).show()
            return
        }

        if (editMdp.text.toString().length < 8) {
            Toast.makeText(applicationContext, "Mot de passe incorrect", Toast.LENGTH_SHORT).show()
            return
        }

        auth!!.signInWithEmailAndPassword(
            editMail.text.toString(),
            editMdp.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth!!.currentUser
                    updateUI(user)
                } else {

                    updateUI(null)
                }
            }
        }

    private fun updateUI(currentUser: FirebaseUser?) {

        if (currentUser != null) {
            if(currentUser.isEmailVerified) {
                val intent = Intent(this, MainActivityWelcome::class.java)
                startActivity(intent)
                this.finish()
            }else{
                Toast.makeText(
                    baseContext, "Merci de vérifier votre adresse mail",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            Toast.makeText(
                baseContext, "Mot de passe et/ou email invalide",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth!!.currentUser
        updateUI(currentUser)
    }
}