package com.example.AppliWeb

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class MainActivityCreateEvenement : AppCompatActivity() {

    private lateinit var db: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_create_evenement)
        db = FirebaseFirestore.getInstance()

        var btnRetour = findViewById(R.id.buttonRetour) as Button
        var btnValid = findViewById(R.id.buttonValid) as Button

        var editVille = findViewById(R.id.editTextVille) as EditText
        editVille.setText(intent.getStringExtra("nameVille"))

        var editTextName = findViewById(R.id.editTextName) as EditText
        var editTextPlace = findViewById(R.id.editTextPlace) as EditText
        var editTextDate = findViewById(R.id.editTextDate) as EditText
        var editTextDescri = findViewById(R.id.editTextDescri) as EditText


        btnRetour.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivityEvenements::class.java)
            intent.putExtra("nameVille", editVille.text.toString())
            startActivity(intent)
        }


        btnValid.setOnClickListener {
            val evenement = hashMapOf(
                "name" to editTextName.text.toString(),
                "lieu" to editTextPlace.text.toString(),
                "date" to editTextDate.text.toString(),
                "description" to editTextDescri.text.toString()
            )
            db.collection("villes")
                .document(editVille.text.toString())
                .collection("evenements")
                .document(editTextName.text.toString()).set(evenement)
                .addOnSuccessListener {
                    Log.d("success", "DocumentSnapshot successfully written!")
                }
                .addOnFailureListener {
                        e -> Log.w("error", "Error writing document", e)
                }
            this.finish()
            val intent = Intent(this, MainActivityEvenements::class.java)
            intent.putExtra("nameVille", editVille.text.toString())
            startActivity(intent)
        }
    }
}