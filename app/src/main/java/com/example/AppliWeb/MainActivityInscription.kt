package com.example.AppliWeb

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.FirebaseAuth


class MainActivityInscription : AppCompatActivity() {

    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_inscription)
        auth = FirebaseAuth.getInstance()

        var btnRetour = findViewById(R.id.buttonRetour) as Button
        var btnValid = findViewById(R.id.buttonValid) as Button


        btnRetour.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnValid.setOnClickListener {
            // verifier si mdp et verif sont égaux et inserer sur firebase
            signUpUser()
//            Toast.makeText(
//                this,
//                "Vous êtes inscrit !",
//                Toast.LENGTH_LONG
//            ).show()
//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
//            this.finish()
        }
    }

    private fun updateUI(currentUser: FirebaseUser?) {

        if (currentUser != null) {
            if(currentUser.isEmailVerified) {
                //strat activity accueil
            }else{
                Toast.makeText(
                    baseContext, "Merci de vérifier votre adresse mail",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            Toast.makeText(
                baseContext, "Mot de passe et/ou email invalide",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth!!.currentUser
        updateUI(currentUser)
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun signUpUser() {

        var editMail = findViewById(R.id.editTextName) as EditText
        var editMdp = findViewById(R.id.editTextMDP) as EditText
        var editVerif = findViewById(R.id.editTextVerif) as EditText

        val emailVerify: Boolean = isEmailValid(editMail.text.toString())

        if (emailVerify){
            if (editMdp.text.toString().length >= 8){
                if (editMdp.text.toString() == editVerif.text.toString()){
                    auth!!.createUserWithEmailAndPassword(editMail.text.toString(), editMdp.text.toString()).addOnCompleteListener(this) { task ->
                        if (task.isSuccessful){
                            val user = auth!!.currentUser
                            user?.sendEmailVerification()?.addOnCompleteListener{task ->
                                if (task.isSuccessful){
                                    val intent = Intent(this, MainActivityWelcome::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                            }
                        } else {
                            Toast.makeText(applicationContext, "Inscription échouée, merci de réessayer plus tard", Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(applicationContext, "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(applicationContext, "Merci de respeter les consignes pour votre mot de passe", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(applicationContext, "Merci de rentrer une adresse mail valide", Toast.LENGTH_SHORT).show()
        }
    }


}