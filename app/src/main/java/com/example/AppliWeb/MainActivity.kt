package com.example.AppliWeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button_conn = findViewById(R.id.buttonConn) as Button
        var button_ins = findViewById(R.id.buttonIns) as Button

        button_conn.setOnClickListener {
            val intent = Intent(this, MainActivityConnexion::class.java)
            //intent.putExtra("nbrModif", nbrModif.text)
            startActivity(intent)
            this.finish()
        }

        button_ins.setOnClickListener {
            val intent = Intent(this, MainActivityInscription::class.java)
            //intent.putExtra("nbrModif", nbrModif.text)
            startActivity(intent)
            this.finish()
        }
    }
}
