package com.example.AppliWeb

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.activity_main_map.*


class MainActivityMap : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_main_map)

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            mapboxMap.setStyle(Style.MAPBOX_STREETS) {
                // Map is set up and the style has loaded. Now you can add data or make other map adjustments
                mapboxMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(LatLng(37.7749, -122.4194), 12.0)
                )
            }
        }

    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume();
        mapView.onResume();
    }

    override fun onPause() {
        super.onPause();
        mapView.onPause();
    }

    override fun onStop() {
        super.onStop();
        mapView.onStop();
    }

    override fun onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    override fun onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }
}