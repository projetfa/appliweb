package com.example.AppliWeb

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore

class MainActivityEvenements : AppCompatActivity(){

    private lateinit var db: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_evenements)
        db = FirebaseFirestore.getInstance()

        var txtView = findViewById(R.id.textView) as TextView
        txtView.text = intent.getStringExtra("nameVille")

        var txtViewEv = findViewById(R.id.textViewEv) as TextView

        var btnRetour = findViewById(R.id.buttonRetour) as Button
        var btnCreate = findViewById(R.id.buttonCreate) as Button
        var spinner = findViewById(R.id.spinner) as Spinner

        var txtViewName = findViewById(R.id.textViewName) as TextView
        var txtViewAdresse = findViewById(R.id.textViewAdresse) as TextView
        var txtViewDate = findViewById(R.id.textViewDate) as TextView
        var txtViewDescription = findViewById(R.id.textViewDescription) as TextView

        db.collection("villes")
            .document(txtView.text.toString())
            .collection("evenements")
            .get()
            .addOnSuccessListener { result ->
                val colors = Array(result.size()) { _ -> "Aucune expedition" }
                var i = 0
                for (document in result) {
                    Log.d("result", "${document.id} => ${document.data}")
                    document.getString("name")?.let {colors.set(i, it)}
                    i++
                }
                // Initializing an ArrayAdapter
                val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, colors)
                // Set the drop down view resource
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Finally, data bind the spinner object with adapter
                spinner.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.d("error", "Error getting documents: ", exception)
            }


        // Set an on item selected listener for spinner object
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                txtViewEv.text = parent.getItemAtPosition(position).toString()

                db.collection("villes")
                    .document(txtView.text.toString())
                    .collection("evenements")
                    .document(txtViewEv.text.toString())
                    .get()
                    .addOnSuccessListener { result ->
                        if (result != null) {
                            Log.d("exist", "DocumentSnapshot data: ${result.data}")
                            txtViewName.text = result.getString("name")
                            txtViewAdresse.text = result.getString("lieu")
                            txtViewDate.text = "Le " + result.getString("date")
                            txtViewDescription.text = result.getString("description")
                        } else {
                            Log.d("noexist", "No such document")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d("error", "Error getting documents: ", exception)
                    }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Another interface callback
            }
        }

        btnRetour.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivityWelcome::class.java)
            startActivity(intent)
        }

        btnCreate.setOnClickListener {
            this.finish()
            val intent = Intent(this, MainActivityCreateEvenement::class.java)
            intent.putExtra("nameVille", txtView.text.toString())
            startActivity(intent)
        }
    }
}